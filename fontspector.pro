#-------------------------------------------------
#
# Project created by QtCreator 2017-10-15T23:42:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets 

TARGET = fontspector
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RESOURCES     = resources.qrc
SOURCES += \
        main.cpp \
    fontspector.cpp \
    flowlayout.cpp \
    renderarea.cpp \
    renderitem.cpp

HEADERS += \
    fontspector.h \
    flowlayout.h \
    renderarea.h \
    renderitem.h

FORMS += \
        fontspector.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/ -lfreetype281_
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lfreetype281_d

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/libfreetype281_.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/libfreetype281_d.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/freetype281_.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/freetype281_d.lib


unix:!macx: LIBS += -lfreetype
macx: LIBS += -L$$PWD/lib/ -lfreetype -lz -lpng -lbz2

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

macx: PRE_TARGETDEPS += $$PWD/lib/libfreetype.a

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

#macx: PRE_TARGETDEPS += $$PWD/lib/libpng.a

#macx: LIBS += -L$$PWD/lib/ -lbz2

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

#macx: PRE_TARGETDEPS += $$PWD/lib/libbz2.a
