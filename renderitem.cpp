#include "renderitem.h"

RenderItem::RenderItem(std::shared_ptr<QImage> tmpImage): m_preview (tmpImage)
{
    
}

RenderItem::RenderItem(std::shared_ptr<QImage> tmpImage, QString &tmpPath): m_preview(tmpImage), m_path(tmpPath)
{
    
}

QString RenderItem::getPath() const
{
    return m_path;
}

void RenderItem::setPath(const QString &value)
{
    m_path = value;
}

std::shared_ptr<QImage> RenderItem::getPreview() const
{
    return m_preview;
}

void RenderItem::setPreview(const std::shared_ptr<QImage> &value)
{
    m_preview = value;
}
