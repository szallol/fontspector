#include "fontspector.h"

#include "ui_fontspector.h"
#include "renderarea.h"
#include "renderitem.h"

#include <iterator>

#include <QMessageBox>
#include <QPainter>
#include <QFileDialog>
#include <QFuture>
#include <QtConcurrent/QtConcurrentRun>


static FT_Library ftLib;
static bool abortFileScan = false;
static bool filescanFinished = true;

#define TRUNC(x) ((x) >> 6)

struct glyphData {
	int pen_x;
	int pen_y;
	QPixmap pixmapGlyph;
	QRect glyphRect;
};

FontSpector::FontSpector(QWidget *parent) : QMainWindow(parent), m_ui(new Ui::FontSpector)
{
	m_searchRecursive = true;

	m_nrFonts = 0;
	m_showFrom = 0;
    
	auto error = FT_Init_FreeType(&ftLib);
	if ( error )
	{
		QMessageBox msgBox;
		msgBox.setText("Error init FreeType library!");
		msgBox.exec();
	}

	m_ui->setupUi(this);
    
    setWindowTitle("fontSpector beta 0.1 (szallol@gmail.com)");
	setWindowIcon(QIcon(QLatin1String(":/font.ico"))); 
    
    connect(this, &FontSpector::setEnableDisableNavButtons, this, [&] (bool newStatus) {
		m_ui->frameNavButtons->setEnabled(newStatus); // disable navigation buttons
    });
    
    // update button
    connect(m_ui->btnUpdateText, &QPushButton::clicked, [&] () {
        updateFilesList();
    });
    
	// refresh layout with next page (previews should be allready cached)
	connect(m_ui->pushButtonNextPage, SIGNAL(clicked(bool)), this, SLOT(nextPage()));
	connect(m_ui->pushButtonPrevPage, SIGNAL(clicked(bool)), this, SLOT(prevPage()));
	connect(m_ui->pushButtonFirstPage, SIGNAL(clicked(bool)), this, SLOT(firstPage()));
	connect(m_ui->pushButtonLastPage, SIGNAL(clicked(bool)), this, SLOT(lastPage()));

	// set/unset search subdirectories recursive
	connect(m_ui->checkBoxRecursiveSearch, SIGNAL(toggled(bool)), this, SLOT(searchRecursive(bool)));

	// change dir clicked
	connect(m_ui->buttonSelectDir, SIGNAL(clicked(bool)), this, SLOT(changeDirectory())); 

	// work directory changed
	connect(m_ui->lineSelectedDir, &QLineEdit::textChanged, [&] () {
		updateFilesList();
		//		emit fontListChanged();
	});

	// update display text
	connect(m_ui->lineDisplayText, &QLineEdit::textChanged, this, [&] (QString text) {
		m_displayText = text;
	});

	// async add new RenderArea
	connect(this, SIGNAL(generateNewAreaWidget(unsigned)), this, SLOT(addRenderArea(unsigned)));

	//if font limit leached
	connect(this, SIGNAL(updateDetailInfo()), this, SLOT(refreshDetailInfo()));

	// cahnge black/white
//	connect(m_ui->pushButtonBlackWhite, &QPushButton::clicked, [&] () {
//		//	    bool bwChecked = m_ui->checkBoxBlackWhite->isChecked();
//        std::for_each(m_previewsSaved.begin(), m_previewsSaved.end(), [] (std::shared_ptr<QImage> tmpGlyphs) {
//            tmpGlyphs->invertPixels();
//        });
////		std::for_each (m_renders.begin(), m_renders.end(), [this] (RenderArea *render) {
////			render->setBlackWhite();
////			render->update();
////		});
//	});

	m_flowLayout = new FlowLayout (-1, 10, 10);
	m_ui->scrollAreaWidgetContents->setLayout(m_flowLayout);

	m_flowLayoutSaved = new FlowLayout (-1, 10, 10);
	m_ui->scrollAreaSelectedWidgets->setLayout(m_flowLayoutSaved);
	
	m_displayText = m_ui->lineDisplayText->text();
    
    emit setEnableDisableNavButtons(false);
}

FontSpector::~FontSpector()
{
	abortFileScan = true;
	while (!filescanFinished) {
		QThread::msleep(10); 
	}

	m_flowLayout->removeAllItems();
	m_flowLayoutSaved->removeAllItems();

	delete m_flowLayout;
	delete m_flowLayoutSaved;
	delete m_ui;
}

void FontSpector::findConcurrent(const QString &path, const QStringList &patterns)
{
	// mechanism to stop QtConcurrent
	if (abortFileScan) {
		return;
	}

	filescanFinished = false;
	
	QDir currentDir(path);
	const QString prefix = path + QLatin1Char('/');
	QVector<QFuture<void>> futures;

	// find subdirectoryes
	if (m_searchRecursive) {
		foreach (const QString &dir, currentDir.entryList(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot))
		{
			QString dirPath = prefix + dir;
			futures.append(QtConcurrent::run([dirPath, patterns, this] () {


				findConcurrent(dirPath, patterns);
				QThread::msleep(10);
			}));
		}
	}

	//find files
    
    {
		std::lock_guard<std::mutex>	lock(m_mutexNewRender);
		foreach (const QString &match, currentDir.entryList(patterns, QDir::Files | QDir::NoSymLinks, QDir::Name))
		{
			QString fontPath = prefix + match;

			std::shared_ptr<QImage> pTmpCache = generateFontPreview(fontPath, m_ui->spinFontSize->value(), 300, 200);
			if (pTmpCache) {

				std::shared_ptr<RenderItem> shTmpItem = std::make_shared<RenderItem>(pTmpCache, fontPath);

				m_renderItems.push_back(shTmpItem);
				m_nrFonts++;

				if (m_nrFonts <= MAX_DISPLAY_FONTS) {
					emit generateNewAreaWidget((unsigned) m_renderItems.size() - 1);
				}

				if (m_nrFonts > (unsigned)m_renderAreas.size()) {
					emit setEnableDisableNavButtons(true);
				}

				emit updateDetailInfo();
			}
		}
    }

    
	// wait for background jobs
	foreach (QFuture<void> future, futures) {
		future.waitForFinished(); 
	}
}

void FontSpector::changeDirectory()
{
	m_findPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "./", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	m_ui->lineSelectedDir->setText(m_findPath);
}

void FontSpector::updateFilesList()
{
	abortFileScan = true;
	while (!filescanFinished) {
		QThread::msleep(10); 
	}
	abortFileScan = false;
	
  	m_flowLayout->removeAllItems();
	m_renderItems.clear();
    m_renderAreas.clear();

	m_nrFonts = 0;
	QtConcurrent::run([this] () {
		findConcurrent(m_findPath, QStringList{"*.ttf", "*.otf", "pfb", "woff"});
		filescanFinished = true;
	});
}

void FontSpector::addRenderArea(unsigned index)
{
	std::shared_ptr<RenderArea> tmpArea(new RenderArea(m_renderItems[index]));

	connect(tmpArea.get(), SIGNAL(addMeToTheSaveList(std::shared_ptr<RenderItem>)), this, SLOT(addToSaveList(std::shared_ptr<RenderItem>)));

	m_renderAreas.push_back(tmpArea);

	m_flowLayout->addWidget(tmpArea.get());
}

void FontSpector::refreshDetailInfo()
{
	int showTo = m_showFrom + ((m_showFrom + MAX_DISPLAY_FONTS) < m_nrFonts ? MAX_DISPLAY_FONTS : m_nrFonts - m_showFrom);
	m_ui->labelPositionDetail->setText(QStringLiteral("[") + QString::number(m_showFrom) + QStringLiteral("-") + QString::number(showTo) + 
	                                   QStringLiteral("] of ") + QString::number(m_nrFonts));
}

void FontSpector::addToSaveList(std::shared_ptr<RenderItem> tmpItem)
{
	std::shared_ptr<RenderArea> tmpArea(new RenderArea(tmpItem, true));
//	auto tmpArea = new  RenderArea(tmpItem, true);

    connect (tmpArea.get(), &RenderArea::doubleClicked, this, [&] (std::shared_ptr<RenderArea> toRemoveArea) {
        m_rendersSaved.erase(m_rendersSaved.find(toRemoveArea));
       	m_flowLayoutSaved->removeAllItems();
		m_flowLayoutSaved->update();
        m_ui->scrollAreaSelectedWidgets->update();
    });
    
    if (!(m_rendersSaved.find(tmpArea) != m_rendersSaved.end())) {
        m_rendersSaved.insert(tmpArea);
		m_flowLayoutSaved->addWidget(tmpArea.get());
		m_flowLayoutSaved->update();
    }
}

void FontSpector::searchRecursive(bool newVal)
{
	m_searchRecursive = newVal;
}

void FontSpector::nextPage()
{
	m_ui->frameNavButtons->setEnabled(false);

	m_showFrom += (m_showFrom + MAX_DISPLAY_FONTS < m_nrFonts ? MAX_DISPLAY_FONTS : 0);
	unsigned iShow = m_showFrom;
	unsigned counter = 0;

	// update widgets from previews
	while (counter < MAX_DISPLAY_FONTS && iShow < m_nrFonts){
		auto tmpRender = m_renderAreas.at(counter);

		tmpRender->setRenderItem(m_renderItems[iShow]);
		counter++;
		iShow++; 
	}

	emit updateDetailInfo();
	m_ui->frameNavButtons->setEnabled(true);
	m_ui->scrollAreaWidgetContents->update();
}

void FontSpector::prevPage()
{
    m_ui->frameNavButtons->setEnabled(false);

	m_showFrom -= ((int)m_showFrom - MAX_DISPLAY_FONTS >= 0 ?  MAX_DISPLAY_FONTS : m_showFrom);
	unsigned iShow = m_showFrom;
	unsigned counter = 0;

	// update widgets from previews
	while (counter < MAX_DISPLAY_FONTS && iShow < m_nrFonts){
		auto tmpRender = m_renderAreas.at(counter);

		tmpRender->setRenderItem(m_renderItems[iShow]);
		counter++;
		iShow++; 
	}

	emit updateDetailInfo();
	m_ui->frameNavButtons->setEnabled(true);
    m_ui->scrollAreaWidgetContents->update();
}

void FontSpector::firstPage()
{
	m_ui->frameNavButtons->setEnabled(false);

	m_showFrom = 0;
	unsigned iShow = m_showFrom;
	unsigned counter = 0;

	// update widgets from previews
	while (counter < MAX_DISPLAY_FONTS && iShow < m_nrFonts){
		auto tmpRender = m_renderAreas.at(counter);

		tmpRender->setRenderItem(m_renderItems[iShow]);
		counter++;
		iShow++; 
	}

	emit updateDetailInfo();
	m_ui->frameNavButtons->setEnabled(true);
	m_ui->scrollAreaWidgetContents->update();}

void FontSpector::lastPage()
{
    m_ui->frameNavButtons->setEnabled(false);

	m_showFrom = m_nrFonts - MAX_DISPLAY_FONTS;
	unsigned iShow = m_showFrom;
	unsigned counter = 0;

	// update widgets from previews
	while (counter < MAX_DISPLAY_FONTS && iShow < m_nrFonts){
		auto tmpRender = m_renderAreas.at(counter);

		tmpRender->setRenderItem(m_renderItems[iShow]);
		counter++;
		iShow++; 
	}

	emit updateDetailInfo();
	m_ui->frameNavButtons->setEnabled(true);
    m_ui->scrollAreaWidgetContents->update();
}

std::shared_ptr<QImage> FontSpector::generateFontPreview(QString &fontfile, unsigned fontSize, unsigned width, unsigned height)
{
	if (fontfile.isEmpty()) {
        return nullptr;
	}

	FT_Face	m_face;
	auto error = FT_New_Face(ftLib, fontfile.toLocal8Bit().data(), 0, &m_face );
	if ( error == FT_Err_Unknown_File_Format )
	{
        return nullptr;
	}
	else if ( error )
	{
        return nullptr;
	}

	error = FT_Set_Char_Size(m_face, 0, (fontSize > 0 ? fontSize : 20) * 64, physicalDpiX(), physicalDpiY());
	if ( error )
	{
        return nullptr;
	}

	FT_GlyphSlot  slot;

	if (m_face) {
		if (m_face->glyph){
			slot = m_face->glyph;
		} else {
            return nullptr;
		}
	} else {
        return nullptr;
	}

	int	pen_x, pen_y, n;
	pen_x = 0;
	pen_y = 0;

	auto array = m_displayText.toLocal8Bit();
	char *text = array.data();
	auto num_chars = array.length();

	QVector<QRgb> colorTable;
	for (int i = 0; i < 256; ++i) {
		colorTable << qRgba(0, 0, 0, i);
	}

	unsigned maxHeight = 0;
	unsigned maxWidth = 0;
	std::vector<glyphData> glyphs;

	for ( n = 0; n < num_chars; n++ )
	{
		FT_UInt  glyph_index;

		/* retrieve glyph index from character code */
		glyph_index = FT_Get_Char_Index( m_face, text[n] );

		QRect m_glyphRect;
		unsigned int glyphTotalHeight=0;

		/* load glyph image into the slot (erase previous one) */
		auto error = FT_Load_Glyph( m_face, glyph_index, FT_LOAD_DEFAULT );
		if ( error )
			continue;  /* ignore errors */
		else
		{
			FT_Pos left = m_face->glyph->metrics.horiBearingX;
			FT_Pos right = left + m_face->glyph->metrics.width;
			FT_Pos top = m_face->glyph->metrics.horiBearingY;
			FT_Pos bottom = top - m_face->glyph->metrics.height;

			glyphTotalHeight = TRUNC(top + bottom) + 1;
			maxWidth += TRUNC(left) + TRUNC(right - left) + 1;

			m_glyphRect = QRect(QPoint(TRUNC(left), -TRUNC(top) + 1), QSize(TRUNC(right - left) + 1, TRUNC(top - bottom) + 1));
		}

		error = FT_Render_Glyph( m_face->glyph, FT_RENDER_MODE_NORMAL );
		if ( error )
			continue;

		// create temp image for every glyph
		QImage tmpGlyph (slot->bitmap.buffer, slot->bitmap.width, slot->bitmap.rows, slot->bitmap.pitch, QImage::Format_Indexed8);
		tmpGlyph.setColorTable(colorTable);

		glyphs.push_back({pen_x, pen_y, QPixmap::fromImage(tmpGlyph) , m_glyphRect});

		/* increment pen position */
		pen_x += slot->advance.x >> 6;
		pen_y += slot->advance.y >> 6; /* not useful for now */

		maxHeight = glyphTotalHeight > maxHeight ? glyphTotalHeight : maxHeight;
	}

	maxWidth += 20;
	maxHeight += 20;

	QSize	hintSize(width, height);
    
	std::shared_ptr<QImage> outImage = std::make_shared<QImage>(hintSize, QImage::Format_RGB16);
	outImage->fill(Qt::white);

	for(auto &tmpGlyph : glyphs) {
		QPainter cachePainter(outImage.get());

		cachePainter.translate(tmpGlyph.glyphRect.x(), tmpGlyph.glyphRect.y());
		cachePainter.drawPixmap((hintSize.width() /2) - (maxWidth /2) + tmpGlyph.pen_x, (hintSize.height() /2)  - tmpGlyph.pen_y, tmpGlyph.pixmapGlyph);
	} 

	// draw font name to the bottom of the widget
	auto fontName = FT_Get_Postscript_Name(m_face);
	QPainter fontNamePainter(outImage.get());
	fontNamePainter.setRenderHint(QPainter::TextAntialiasing);
	fontNamePainter.setPen(QPen(Qt::black));
	fontNamePainter.drawText(10, hintSize.height() - 10, QString::fromLatin1(fontName));

	if (m_face) {
		FT_Done_Face(m_face);
	}
  
    return outImage;
}

