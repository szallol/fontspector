#include "renderarea.h"

#include <QDrag>

RenderArea::RenderArea(std::shared_ptr<RenderItem> tmpRenderItem, bool tmpSaveList) : m_renderItem(tmpRenderItem), m_isSaveList(tmpSaveList)
{
	m_cacheSet = true;
    
	m_hintSize = tmpRenderItem->getPreview()->size();

    resize(m_hintSize);
    
    setToolTip(tmpRenderItem->getPath());
}

RenderArea::~RenderArea()
{
}

QSize RenderArea::sizeHint() const
{
	return m_hintSize;
}

void RenderArea::setFontSize(unsigned int inFontSize)
{
	m_fontSize = inFontSize;
}

void RenderArea::setBlackWhite()
{
	m_invert = !m_invert;
    m_renderItem->getPreview()->invertPixels();
}

QSize RenderArea::minimumSizeHint() const
{
	return m_hintSize;
}

void RenderArea::paintEvent(QPaintEvent * /* event */)
{
	if (m_cacheSet) {
		QPainter painter(this);
		painter.drawImage(0, 0, *m_renderItem->getPreview());	
	}
}

void RenderArea::mouseDoubleClickEvent(QMouseEvent *)
{
//    if (!m_isSaveList) {
		emit addMeToTheSaveList(m_renderItem);
//    } else {
//		emit doubleClicked(shared_from_this());
//    }
}

std::shared_ptr<RenderItem> RenderArea::renderItem() const
{
    return m_renderItem;
}

void RenderArea::setRenderItem(const std::shared_ptr<RenderItem> &renderItem)
{
    m_renderItem = renderItem;
}
