#include "fontspector.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setApplicationName("fontSpector beta 0.1 (szallol@gmail.com)");
	QApplication::setWindowIcon(QIcon(QLatin1String(":/font.ico"))); 

    FontSpector w;
    w.show();

    return a.exec();
}
