#ifndef FONTSPECTOR_H
#define FONTSPECTOR_H

#include "ft2build.h"
#include FT_FREETYPE_H

#include "flowlayout.h"
#include "renderitem.h"

#include <memory>
#include <mutex>
#include <set>

#include <QMainWindow>
#include <QPen>
#include <QImage>
#include <QPoint>
#include <QMap>
#include <QMimeData>
#include <QDragEnterEvent>

#define MAX_DISPLAY_FONTS 300

namespace Ui {
class FontSpector;
}

class RenderArea;

class FontSpector : public QMainWindow
{
	Q_OBJECT

public:
	explicit FontSpector(QWidget *parent = 0);
	~FontSpector();

	void findRecursion(const QString &path, const QStringList &patterns);
	void findConcurrent(const QString &path, const QStringList &patterns);

public slots:
	void changeDirectory();
	void updateFilesList();
	void addRenderArea(unsigned);
	void refreshDetailInfo();
	void addToSaveList(std::shared_ptr<RenderItem>);
	void searchRecursive(bool);

	void nextPage();
	void prevPage();
	void firstPage();
	void lastPage();
    
    std::shared_ptr<QImage> generateFontPreview(QString&, unsigned, unsigned, unsigned); 

signals:
	void fontListChanged();
	void generateNewAreaWidget(unsigned);
	void updateDetailInfo();
    void setEnableDisableNavButtons(bool);

protected:

private:
	unsigned 				m_nrFonts;
	unsigned				m_showFrom;

	bool					m_searchRecursive;
	Ui::FontSpector			*m_ui;

	FlowLayout				*m_flowLayout;
	FlowLayout				*m_flowLayoutSaved;
    
	QString					m_displayText;
	QString					m_findPath;
    std::vector<std::shared_ptr<RenderItem>>	m_renderItems;
    
	QVector<std::shared_ptr<RenderArea>> 	m_renderAreas;
    std::set<std::shared_ptr<RenderArea>>	m_rendersSaved;
    
    std::mutex				m_mutexNewRender;
};

#endif // FONTSPECTOR_H
