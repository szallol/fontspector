#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <renderitem.h>

#include <memory>

#include <QWidget>
#include <QMessageBox>
#include <QPainter>
#include <QMouseEvent>
#include <QMimeData>

class RenderArea : public QWidget
{
	Q_OBJECT

public:
    RenderArea(std::shared_ptr<RenderItem>, bool tmpSaveList = false);

	~RenderArea();

	QSize minimumSizeHint() const override;
	QSize sizeHint() const override;
	void setFontSize(unsigned int);
	void setBlackWhite();

    std::shared_ptr<RenderItem> renderItem() const;
    void setRenderItem(const std::shared_ptr<RenderItem> &renderItem);
    
signals:
    void addMeToTheSaveList(std::shared_ptr<RenderItem>); 
    void doubleClicked(std::shared_ptr<RenderArea>);
    
protected:
    void paintEvent(QPaintEvent *event) override;
	void mouseDoubleClickEvent(QMouseEvent *) override;

private:
	bool			m_invert;
	bool			m_cacheSet;

    std::shared_ptr<RenderItem>	m_renderItem;
    
	QString			m_displayText;
	QString			m_fontFile;
	unsigned int 	m_fontSize;
	QSize			m_hintSize;
    
    bool			m_isSaveList;
};

#endif // RENDERAREA_H
