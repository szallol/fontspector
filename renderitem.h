#ifndef RENDERITEM_H
#define RENDERITEM_H

#include <memory>

#include <QImage>
#include <QString>

class RenderItem
{
public:
    RenderItem(std::shared_ptr<QImage>);
    RenderItem(std::shared_ptr<QImage>, QString&);
    
    QString getPath() const;
    void setPath(const QString &value);
    
    std::shared_ptr<QImage> getPreview() const;
    void setPreview(const std::shared_ptr<QImage> &value);
    
private:
    QString m_path;
    std::shared_ptr<QImage>	m_preview;
};

#endif // RENDERITEM_H
